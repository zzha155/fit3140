'''Contains the Tape class for the Turing Machine Simulator.

Author:
    Robert Merkel <robert.merkel@monash.edu>
'''

from collections import deque

class Tape:
    '''Implements a Turing machine tape.  Tape is infinite in both directions.
'''
    def __init__(self, initialString, blankChar):
        '''Make a new tape.

        Args:

            initialString (str): the initial contents of the tape
            blankChar (str): the blank symbol
        '''
        self.tape=deque(initialString)
        self.pos=0
        self.blankChar = blankChar
        
    def get_symbol(self):
        '''Get the symbol currently under the tape head.


        Returns:
            str: the symbol under the tape
        '''
        return self.tape[self.pos]
    
    def write_left(self, newChar):
        '''Write a new symbol at the current tape position and move left

        Args:
            newChar (str): the symbol to be written
        '''
        self.tape[self.pos] = newChar
        if (self.pos == 0):
            self.tape.appendleft(self.blankChar)
        else:
            self.pos -= 1

    def write_right(self, newChar):
        '''Write a new symbol at the current tape position and move right

        Args:
            newChar (str): symbol to be written
        '''
        self.tape[self.pos] = newChar
        self.pos += 1
        if self.pos == len(self.tape):
            self.tape.append(self.blankChar)
        

    def get_tape(self):
        '''get a string representation of the tape contents and position

        Returns:
            str: contents of the tape
        '''
        charList=[]
        for i in range(0, len(self.tape)):
            if i==self.pos:
                charList.append("[color=#DA70D6][size=40]")
                charList.append(self.tape[i])
                charList.append("[/size][/color]")
            else:
                charList.append(self.tape[i])
        return ''.join(charList)
