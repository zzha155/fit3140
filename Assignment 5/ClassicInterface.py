__author__ = 'Derrick'
"""this module contains all the necessary classes to make the interface run"""

from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.relativelayout import RelativeLayout
from kivy.lang import Builder
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.properties import NumericProperty, StringProperty
from kivy.uix.scatter import Scatter
import xml.etree.ElementTree as ET
import math
from TuringMachineModel import *


Builder.load_file("normalState.kv")
Builder.load_file("initialState.kv")
Builder.load_file("acceptingState.kv")
Builder.load_file("BiTransition.kv")
Builder.load_file("UniTransition.kv")


class ClassicInterface(App):
    """the main class to make the interface run """

    def build(self):
        return ClassicInterfaceFrame()


class Item(RelativeLayout):
    """base class for all the items in the state diagram"""

    name = StringProperty()
    previous_name=StringProperty()

    def __init__(self,*args,**kwargs):
        """constructor"""
        super(Item,self).__init__(**kwargs)
        self.bind(name=self.on_name)
        if args: # get all the file information from the frame
            self.frame = args[0]
            self.tree = self.frame.tree
            self.root = self.frame.root
            self.XML_spec = self.frame.XML_spec

    def on_name(self,*args):
        """would be overrided by its subclasses """
        if self.previous_name :
            for normal_state in self.root.find("states").findall("state"):
                if normal_state.attrib["name"] is self.previous_name :
                    normal_state.set("name",self.name)
                for transition in normal_state.findall("transition"):  # update the state inforamtion in the transitions
                # related to them
                    if transition.attrib["newstate"] == self.previous_name:
                        transition.set("newstate",self.name)
        else:
            parent=self.root.find("states")
            ET.SubElement(parent,"state",{"name":self.name})
        self.tree.write(self.frame.XML_spec.name)


class AcceptingState(Item):
    """accepting state, derived from the class State"""

    def __init__(self,*args,**kwargs):
        """constructor"""
        super(AcceptingState,self).__init__(*args,**kwargs)

    def on_name(self,*args):
        """override its parent's method, when the name of the accepting state changes, it will automatically update the
        xml file """
        super(AcceptingState,self).on_name(*args)
        if self.previous_name :  # if the name already exists, update the information
            for final_state in self.root.find("finalstates").findall("finalstate"):  # loop through
                if final_state.attrib["name"] is self.previous_name :
                    final_state.set("name",self.name)
        else:  # if it is new state, create a new element
            parent=self.root.find("finalstates")
            ET.SubElement(parent,"finalstate",{"name":self.name})
        self.tree.write(self.frame.XML_spec.name)  # insert the information into the xml file


class InitialState(Item):
    """initial state, derived from the class State"""
    def __init__(self,*args,**kwargs):
        super(InitialState,self).__init__(*args,**kwargs)

    def on_name(self,*args):
        """override the parent's method , update initial state information on the change of the name  """
        super(InitialState,self).on_name(*args)
        self.root.find("initialstate").set("name",self.name)
        self.tree.write(self.frame.XML_spec.name)



class NormalState(Item):
    """normal state, derived from the class State"""
    def __init__(self,*args,**kwargs):
        super(NormalState,self).__init__(*args,**kwargs)




class Transition(Item):
    """inherited from the class Item, and it is the base class for different types of transitions"""
    def __init__(self,*args,**kwargs):
        """constructor"""
        super(Transition,self).__init__(*args,**kwargs)
        self.editable_label = None
        self.link_states = []  # the states linked by this transition
        self.symbols = None

    def on_name(self,*args):
        """override its parent's method, and insert the state information on the change of the name """
        start_state = self.link_states[0]
        end_state = self.link_states[1]
        self.symbols = self.name.split(",")
        if len(self.symbols) == 3 and (self.symbols[2] is "R" or self.symbols[2] is "L"):
            previous_symbols = self.previous_name.split(",")
            if len(previous_symbols) == 3:
                for state in self.root.find("states").findall("state"):
                    for tranistion in state.findall("transition"):
                        if tranistion.attrib["seensym"] == previous_symbols[0] and tranistion.attrib["writesym"] == previous_symbols[1] and tranistion.attrib["move"] == previous_symbols[2] and tranistion.attrib["newstate"] == end_state.children[0].name:
                            tranistion.set("seensym",self.symbols[0])
                            tranistion.set("writesym",self.symbols[1])
                            tranistion.set("move",self.symbols[2])
            else:
                for state in self.root.find("states").findall("state"):
                   if state.attrib["name"] is start_state.children[0].name :
                        ET.SubElement(state,"transition",{"seensym":self.symbols[0], "writesym":self.symbols[1], "newstate":end_state.children[0].name ,"move":self.symbols[2]})
            self.tree.write(self.frame.XML_spec.name)
            self.frame.status.content = "[color=#FF00FF]Transition Created [/color]"
        else:
            self.frame.status.content = "[color=#DC143C]Wrong Format[/color]"
            self.editable_label.create_text_input()


class BiTransition(Transition):
    """Transition that connects two states """
    angle = NumericProperty(0)  # the angle between the horizontal line and the transition

    def __init__(self,*args, **kwargs):
        """constructor """
        super(BiTransition, self).__init__(*args,**kwargs)


class UniTransition(Transition):
    """transition that connects itself """
    height_ratio = NumericProperty(2)
    side_distance = NumericProperty(0)
    def __init__(self,*args,**kwargs):
        """constructor"""
        super(UniTransition,self).__init__(*args,**kwargs)


class DraggableWidget(Scatter):
    """customise the our own Scatter class"""

    def __init__(self, **kwargs):
        """constructor"""
        super(DraggableWidget, self).__init__(**kwargs)
        self.bind(pos=self.on_position)
        self.transitions = []  # to store all the transitions that link this widget

    def on_position(self, *args):
        """when the position of the widget changes, it will re-draw the transition"""

        if self.transitions:  # only if there is at least one transition linking this widget
            for transition in self.transitions:  # for each transition this widget is linked by
                frame = self.parent.parent
                frame.stateDiagram.remove_widget(transition)  # remove the transition from the state diagram
                text = transition.children[0].text  # extra the text from the previous transition
                start_state = transition.link_states[0]
                end_state = transition.link_states[1]
                new_transition = frame.draw_transition(transition.link_states[0], transition.link_states[1])  # draw
                # a new transition
                self.transitions.remove(transition)  # remove the old transition
                self.transitions.append(new_transition)  # add the new transition
                if self is start_state:  # if this widget is the start_state, replace the old transition in the
                    # end_state with the new transition
                    end_state.transitions.append(new_transition)
                    end_state.transitions.remove(transition)
                else:  # this is the end_state, replace the old transition in the start_state with the new transition
                    start_state.transitions.append(new_transition)
                    start_state.transitions.remove(transition)
                new_transition.children[0].text = text  # write the text to the new transition

    def on_touch_down(self, touch):
        """when the user press the widget"""

        frame = self.parent.parent
        if self.collide_point(touch.x, touch.y) and frame.get_if_to_link_state() is True:  # only if when the user has pressed
            # the transition button
            frame.selected_states.append(self)  # add the state to the list
            return True
        return super(DraggableWidget, self).on_touch_down(touch)


class EditableLabel(Label):
    """the text of the label can be edited through textInput , when user double click the label, the textInput frame
     will pop up, and when the user finishes editing and click on 'enter', the input frame will disappear  """

    def __init__(self, **kwargs):
        """constructor"""

        super(EditableLabel, self).__init__(**kwargs)
        self.text_input = None
        self.font_size=25
        self.color = (0, 0, 0, 1)  # set the font color to black

    def on_touch_down(self, touch):
        """when the user click on the label, this function would be trigger"""
        if self.collide_point(touch.x, touch.y) and touch.is_double_tap:  # only if it is double-tapped
            self.create_text_input()
            return True
        return super(EditableLabel, self).on_touch_down(touch)

    def on_unfocus(self, instance, value):
        """this function is triggered when the focus of the text input is changed """
        if not value:  # when the text input is de-focused
            self.remove_widget(self.text_input)
            self.text_input = None
            self.parent.previous_name = self.text
            self.text = instance.text  # display the user input through label
            self.parent.name = instance.text

    def create_text_input(self):
        """make the text input appear"""
        self.text_input = TextInput(multiline=False, text=self.text, focus=True, pos=(self.x, self.y),
                                        size=(self.width, self.height))
        self.text_input.bind(focus=self.on_unfocus)
        self.add_widget(self.text_input)



class ClassicInterfaceFrame(BoxLayout):
    """the frame for the interface"""

    def __init__(self, **kwargs):
        """the constructor"""
        super(ClassicInterfaceFrame, self).__init__(**kwargs)
        self.selected_states = []
        self.if_to_link_state = False  # to see if the user is selecting two states to make a transition
        self.root = None
        self.tree = None
        self.XML_spec = self.create_new_file()
        self.status.content = "Welcome"
        self.status.run_status = "Unknown Yet"
        self.title.content = "Turing Machine Simulator"
        self.model = None


    def set_status_action(self,content):
        """change the action in the status bar to inform the user what action has been taken"""
        self.status.content = content

    def get_if_to_link_state(self):
        return self.if_to_link_state

    def set_if_to_link_state(self, boolean):
        self.if_to_link_state = boolean

    def create_new_file(self):
        """create a new file and return the file pointer to the file"""
        self.XML_spec = open("TuringMachine.xml", "w")
        with open("templateXML.xml") as template:
            with open("TuringMachine.XML", "w") as XML_spec:
                for line in template:
                    XML_spec.write(line)
        template.close()
        self.tree = ET.parse(self.XML_spec.name)
        self.root = self.tree.getroot()
        return XML_spec

    def edit_tape(self,instance,value):
        """the function is triggered when the tape content is modified"""
        self.root.find("initialtape").text= instance.text
        self.tree.write(self.XML_spec.name)

    def on_load(self, instance):
        """when function is triggered when button Load is pressed , and the user can choose the file through
        file open dialog """
        pass

    def on_new_TM(self, instance):
        """clear the state diagram and create a new XML file """
        self.stateDiagram.canvas.clear()  # clear the state diagram
        self.XML_spec = self.create_new_file()  # create a new file
        self.status.content = "New TM created"
        self.title.content = "Turing Machine Simulator"
        self.status.run_status = "Unknown Yet"
        self.tape.children[0].text = "Double Click Me To Edit Tape"

    def on_save(self, instance):
        pass

    def on_run(self, instance):
        """run the turing machine to completion"""

        self.model = parse_turing_machine(self.XML_spec.name)
        #self.model = parse_turing_machine("flipper.xml")
        if self.model is False:
            self.status.content = "Information Not Enough"
        else:
            self.model.run_to_halt()
            self.status.run_status = self.model.result
            self.status.content = "TM run"
            self.title.content = self.model.get_tape()


    def next_step(self,instance):
        """run the turing machine step by step"""
        if not self.model:  # when the model is not created
            self.model = parse_turing_machine(self.XML_spec.name)
            self.if_run_once = False
        if self.model is False:  # if not all the necessary information is filled out
            self.status.content = "Information Not Enough"
        else:  # run the machine step by step
            if self.if_run_once is False:
                self.status.content = "Next Step"
                self.title.content = self.model.get_tape()
                self.if_run_once = True
            else:
                if self.model.run_in_steps():  # if there is any step left
                    self.status.content = "Next Step"
                    self.title.content = self.model.get_tape()  # display the tape on the screen
                else:
                    self.status.run_status = self.model.result
                    self.status.content = "Done"
                    self.title.content = "Turing Machine Simulator"
                    self.model = None  # reset the model, so a new model would be loaded next time this function is triggered

    def link_states(self, dt):
        """draw a transition between two chosen states """

        if not len(self.selected_states) == 2:  # if no two states have been chosen
            if len(self.selected_states) == 1:
                self.status.content = "Choose second state"

            return True
        else:  # when there are two state selected , link the states up by transition
            state1 = self.selected_states[0]
            state2 = self.selected_states[1]
            if state1 is not state2:  # when they are different states
                transition = self.draw_transition(state1, state2)  # draw the transition
                state1.transitions.append(transition)
                state2.transitions.append(transition)
            else:  # when they are the same state
                num = len(state1.children)  # the number of children in state1
                transition = UniTransition(self,size=(state1.width,state1.height),height_ratio=num+1,side_distance=num-1)
                transition.link_states.append(state1)
                transition.link_states.append(state2)
                state1.add_widget(transition,len(state1.children))
                editable_label = EditableLabel(size=transition.size,pos=(0,state1.height * num))
                transition.add_widget(editable_label)
                transition.editable_label = editable_label
            transition.editable_label.create_text_input()
            self.selected_states = []  # set the list to empty list
            self.set_if_to_link_state(False)
            self.status.content = "Welcome"
            return False

    def calculate_states_coordinates(self, state1, state2):
        """return a list of the difference of x-coordinate, y-coordinate, and the distance between two states' center
        """
        (x1, y1) = state1.center
        (x2, y2) = state2.center
        x = (x1 - x2) ** 2
        y = (y1 - y2) ** 2
        distance = math.sqrt(x + y)
        return [(x2 - x1), (y2 - y1), distance]

    def draw_transition(self, state1, state2):
        """to draw the transition on the state diagram"""

        coordinate_list = self.calculate_states_coordinates(state1, state2)
        angle = 180 / math.pi * math.atan2(coordinate_list[1],coordinate_list[0])  # the angle between the transition
        # and horizontal line
        transition = BiTransition(self,angle=angle, size_hint=(None, None))
        transition.link_states.append(state1)
        transition.link_states.append(state2)
        transition.size = (coordinate_list[2], 40)  # set the size of the transition
        transition.pos = (state1.center_x, state1.center_y - state1.height / 2)  # set the position of the transition
        editable_label = EditableLabel(size_hint=(None, None), size=(50, 30), pos=(
            coordinate_list[0]/2 - 25, coordinate_list[1] / 2))
        transition.add_widget(editable_label)
        transition.editable_label = editable_label
        self.stateDiagram.add_widget(transition)
        return transition


    def create_draggable_widget(self, Object):
        """create a draggable widget . this function accept any widget as parameter and wrap it in the draggableWidget
        so it becomes draggable """
        widget = DraggableWidget(do_scale=False, do_rotation=False)
        widget.size_hint = (None, None)
        widget.size = Object.size
        editable_label = EditableLabel(size=widget.size)
        Object.add_widget(editable_label)
        widget.add_widget(Object)
        self.stateDiagram.add_widget(widget)
        editable_label.create_text_input()  # open the text input when the new widget is created

