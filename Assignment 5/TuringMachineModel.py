#!/usr/bin/python2
'''The core of the Turing Machine simulator:
Author:
    Robert Merkel <robert.merkel@monash.edu>
'''


import xml.etree.ElementTree as ET
import sys

from Transition import Transition
from State import State
from Tape import Tape

# experimental code, commented out.
##class noTransition(Exception):
##    '''Class to represent a 
##    def __init__(self, state, symbol, tape):
##        self.state=state
##        self.symbol=symbol
        
class TuringMachineModel:
    '''Class to represent a Turing machine'''
    
    def __init__(self,  initialState, initialTape, finalStates, blank):
        '''Construct a Turing machine

        Args:

            initialstate (str): The name of the initial state
            initialtape (str): The initial contents of the state
            finalstates (List): A list of the names of the final states
            blank (str): The blank symbol for this TM
        '''
        self.initialState = initialState
        self.states = {}
        self.currentState = self.initialState
        self.tape = Tape(initialTape, blank)
        self.finalStates = finalStates
        self.result = None
        
    def add_state(self,stateName, state):
        '''Add a state to the TM
        Args:
            statename(str) : name of the state
            state(State) : the state
        '''
        
        self.states[stateName] = state

    def get_state(self):
        '''Get the current state

        Returns:
            str: the name of the current state
        '''
        return self.currentState

    def get_tape(self):
        '''Get the current tape as a string

        Returns:
            str: the current tape
        '''
        return self.tape.get_tape()

    def step(self):
        '''Executes one execution step on the TM

        Returns:
            bool: False if there was no transition defined, True otherwise
        '''
        currentSymbol = self.tape.get_symbol()
        state= self.states[self.currentState]
        transition = state.get_transition(currentSymbol)
       # print transition
        if transition is None:
            return False
        self.currentState = transition.get_next_state()
        if (transition.get_next_direction() == "R"):
            self.tape.write_right(transition.get_write_symbol())
        else:
            self.tape.write_left(transition.get_write_symbol())

        return True

    def run_to_halt(self):
        '''Run the machine to completion.  Prints an execution trace
        Returns:
            nothing
        '''

        while self.step():
            pass
        self.if_final_state()


    def run_in_steps(self):
        """run the machine step by step, return False if the machine halts, return True otherwise"""
        if self.step():
            return True
        else:
            self.if_final_state()
            return False

    def if_final_state(self):
        """check if the current state is one of the final state"""
        if self.currentState in self.finalStates:
            self.result = "Halted with Yes"
        else:
            self.result = "Halted with No"
            
            
def parse_turing_machine(infile):
    '''Parses a Turing machine from an XML file

    Args:
        infile(str): name of an XML input file
    Returns:
        A TuringMachine
    '''
    ep = ET.parse(infile)
    tm = ep.getroot()
  #  tm = etree.find('turingmachine')
    if tm.find("initialstate").attrib and tm.find("initialtape").text and tm.findall("finalstates/finalstate") and tm.findall("states/state"):
       # alpha=tm.find("alphabet").text
        tape=tm.find("initialtape").text
        blank=tm.find("blank").attrib['char']
        initialState=tm.find("initialstate").attrib['name']

        finalStates=set()
        fs=tm.findall("finalstates/finalstate")
        for state in fs:
            finalStates.add(state.attrib['name'])

        turingMachineObject = TuringMachineModel(initialState, tape, finalStates, blank)

        states = tm.findall("states/state")
        for state in states:
            stateName = state.attrib['name']
            stateObj = State()
            transitions = state.findall("transition")
            for transition in transitions:
                stateObj.add_transition(transition.attrib['seensym'],
                                        transition.attrib['writesym'],
                                        transition.attrib['newstate'],
                                        transition.attrib['move'])
            turingMachineObject.add_state(stateName, stateObj)


        return turingMachineObject
    else:
        return False


    

