'''Contains the State class for the Turing Machine Simulator.

Author:
    Robert Merkel <robert.merkel@monash.edu>
'''

from Transition import Transition

class State:
    '''Represents a state within a Turing Machine.'''
    
    def __init__(self):
        '''Create a new State with no transitions defined'''
        self.transitions={}

    def add_transition(self, seenSymbol, writeSymbol, newState, move):
        '''Add a transition to this state

        Args:
            seenSymbol (char) : the symbol under the tape to trigger the transition
            writeSymbol (char): the new symbol to write
            newState (char): the name of the state to transition to
            move (char): the direction to move ("L" or "R")
        '''
        transition=Transition(writeSymbol, newState, move)
        self.transitions[seenSymbol] = transition

    def get_transition(self, seenSymbol):
        ''' Get the appropriate transition to follow when you see seenSymbol

        Args:
            seenSymbol (char): the symbol we have seen

        Returns:
            the appropriate transition, or None if no transition defined
        '''
        if seenSymbol in self.transitions:
            return self.transitions[seenSymbol]
        else:
            return None
